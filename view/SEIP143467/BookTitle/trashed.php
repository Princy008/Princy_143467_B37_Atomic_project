<?php
require_once("../../../vendor/autoload.php");



//use App\BITM\SEIP128778\BookTitle;

$obj= new \App\BITM\SEIP143467\BookTitle\BookTitle();


$all_books= $obj->trashed();
######################## pagination code block#1 of 2 start ######################################
$recordCount= count($all_books);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $obj->trashPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;


####################### pagination code block#1 of 2 end #########################################


?>
<!--table-->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title> </title>

    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/font-awesome.min.css">


    <script src="../../../resource/Bootstrap/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/Bootstrap/js/bootstrap.min.js"></script>

    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resource/Bootstrap/js/jquery-ui.js">
    <script src="../../../resource/Bootstrap/js/jquery-1.12.4.js"></script>
    <script src="../../../resource/Bootstrap/js/jquery-ui.js"></script>

    <!-- <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css"> -->
</head>

<style>
    .main{
        margin-top: 10%;
        margin-left: 15%;
        margin-right:15%;
        background-color: #679a9f;



    }
    body{
        background-image:url("../../../resource/assets/images/book.jpg");

        background-repeat:no-repeat;
        background-size: 100% 925px;

    }



</style>

<body  >
<div class="container ">

    <div class="main">



        <div class="panel panel-default" >
            <div class="panel-heading">
                <div class="panel-heading">
                    <h1 style="text-align: center"> BookTitle  Trashed List</h1>


                </div>
            </div>





            <div class="panel-body">
                <form action="recovermultiple.php" method="post" id="multiple">
                    <div class="table-responsive" >
                        </br></br></br></br>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Select all  <input id="select_all" type="checkbox" value="select all"></th>
                                <th>#</th>
                                <th>ID</th>
                                <th>Book title</th>
                                <th>Author</th>

                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <?php
                                $serial=0;

                                foreach($someData as $book){
                                $serial++; ?>
                                <td><input type="checkbox" class="checkbox" name="mark[]" value="<?php echo $book['id']?>"></td>
                                <td><?php echo $serial?></td>
                                <td><?php echo $book['id']?></td>
                                <td><?php echo $book['book_title']?></td>
                                <td><?php echo $book['author_name']?></td>
                                <td>
                                    <a href="recover.php?id=<?php echo $book['id'] ?>"  class="btn btn-info" role="button">Recover</a> &nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="delete.php?id=<?php echo $book['id'] ?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>&nbsp;&nbsp;&nbsp;

                                </td>

                            </tr>
                            <?php }?>


                            </tbody>

                            <a href="index.php"  class="btn btn-primary" role="button">Home</a> &nbsp;&nbsp;&nbsp;&nbsp;

                            <button type="submit" class="btn btn-info">Recover Selected</button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="button" class="btn btn-primary" id="delete">Delete all Selected</button>
                        </table>
                        <!--  ######################## pagination code block#2 of 2 start ###################################### -->
                        <div align="left" class="container">
                            <ul class="pagination">

                                <?php
                                $previous=$page-1;
                                if($previous>0)
                                {echo "<li><a href='index.php?Page=$previous'>" . "Previous" . '</a></li>';}
                                for($i=1;$i<=$pages;$i++)
                                {
                                    if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                                    else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                                }
                                $next=$page+1;
                                if($next<=$pages)
                                    echo "<li><a href='index.php?Page=$next'>" . "Next" . '</a></li>';
                                ?>

                                <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                                    <?php
                                    if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                                    else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                                    if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                                    else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                                    if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                                    else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                                    if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                                    else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                                    if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                                    else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                                    if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                                    else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                                    ?>
                                </select>
                            </ul>
                        </div>
                        <!--  ######################## pagination code block#2 of 2 end ###################################### -->


                    </div>
                    <form>
            </div>

        </div>

    </div>
</div>
<script>
    $(document).ready(function(){
        $(function() {
            $('#confirmation_message').delay(3000).fadeOut();

        });

    });
    $('#delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });
</script>



<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</body>
</html>



